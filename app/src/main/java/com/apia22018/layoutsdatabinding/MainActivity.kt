package com.apia22018.layoutsdatabinding

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.apia22018.layoutsdatabinding.databinding.MainActivityBinding
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: MainActivityBinding = DataBindingUtil.setContentView(this, R.layout.main_activity)

        val user = User("David", 29, "Sweden")

        binding.user = user
        binding.executePendingBindings()

    }

    fun changeName(view: View){
        nameTextInput.setText("Ida")
    }
}
