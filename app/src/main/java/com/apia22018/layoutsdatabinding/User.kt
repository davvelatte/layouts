package com.apia22018.layoutsdatabinding

import android.databinding.*
import android.support.design.widget.TextInputEditText
import android.widget.TextView

class User(name:String, age: Int, country:String): BaseObservable() {

    /*companion object {
        @BindingAdapter("age")
        @JvmStatic
        fun TextInputEditText.setAge(age: Int) {
            setText(age.toString())
        }
    }*/


    @BindingAdapter("age")
    fun setAge(view: TextView, age: Int) {
        view.text = age.toString()
    }

    @get:Bindable
    var name: String = name
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.name)
            }
        }

    @get:Bindable
    var age: Int = age
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.age)

            }
        }

    @get: Bindable
    var country: String = country
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.country)
            }

        }
}